package labo4;

import static org.junit.Assert.*;
import org.junit.Test;

public class GarageTest {
    @Test
    public void testInsertionsSimples(){
        Garage g = new Garage();
        Voiture v = new Voiture("A","b");
        assertTrue(g.estVide());
        assertFalse(g.estPlein());
        assertTrue(g.ajouter(v));
        assertSame(1,g.nombreDeVoitures());
        assertEquals(v,g.obtenir(1));
        assertTrue(g.contient(v));
        assertFalse(g.estVide());
        assertFalse(g.estPlein());
    }
    
    @Test
    public void testInsertionsGaragePlein(){
        Garage g = new Garage(2);
        Voiture v1 = new Voiture("A","a");
        Voiture v2 = new Voiture("B","b");
        Voiture v3 = new Voiture("C","c");
        assertTrue(g.ajouter(v1));
        assertTrue(g.ajouter(v2));
        assertFalse(g.ajouter(v3));
        
        assertEquals(2,g.nombreDeVoitures());
        assertTrue(g.contient(v1));
        assertTrue(g.contient(v2));
        assertFalse(g.contient(v3));
        assertSame(v1,g.obtenir(1));
        assertSame(v2,g.obtenir(2));
                
        assertEquals(null,g.obtenir(3));
        assertEquals(null,g.obtenir(0));
        
        assertFalse(g.estVide());
        assertTrue(g.estPlein());
        assertFalse(g.ajouter(v2));
    }
    
    @Test
    public void testSuppressions(){
        Garage g = new Garage(3);
        Voiture v1 = new Voiture("A","a");
        Voiture v2 = new Voiture("B","b");
        Voiture v3 = new Voiture("C","c");
        g.ajouter(v1);
        g.ajouter(v2);
        g.ajouter(v3);
        
        assertEquals(3,g.nombreDeVoitures());
        assertTrue(g.supprimerDernier());
        assertEquals(2,g.nombreDeVoitures());
        assertSame(v1,g.obtenir(1));
        assertSame(v2,g.obtenir(2));
        assertFalse(g.contient(v3));
        
        assertTrue(g.supprimerDernier());
        assertTrue(g.supprimerDernier());
        assertTrue(g.estVide());
        assertFalse(g.estPlein());
        assertEquals(0,g.nombreDeVoitures());
        
        assertFalse(g.supprimerDernier());
    }
    
    @Test
    public void testRangement(){
        Garage g = new Garage(4);
        Voiture v1 = new Voiture("A","a");
        Voiture v2 = new Voiture("C","c");
        Voiture v3 = new Voiture("B","b");
        Voiture v4 = new Voiture("A","b");
        g.ajouter(v1);
        g.ajouter(v2);
        g.ajouter(v3);
        g.ajouter(v4);
        g.ranger();
        assertSame(v1,g.obtenir(1));
        assertSame(v4,g.obtenir(2));
        assertSame(v3,g.obtenir(3));
        assertSame(v2,g.obtenir(4));
    }
    
    @Test
    public void testToString(){
        Garage g = new Garage(2);
        Voiture v1 = new Voiture("Citroen","2cv");
        Voiture v2 = new Voiture("Citroen","DS");
        g.ajouter(v1);
        g.ajouter(v2);
        
        String prevu = "Garage : \n  1) "+v1+"\n  2) "+v2+"\n";
        assertEquals(prevu,g.toString());
    }

}
