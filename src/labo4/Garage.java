package labo4;

import java.util.ArrayList;
import java.util.List;

public class Garage {
    int capacite;
    List<Voiture> collectionVoiture = new ArrayList<>();

    public Garage() {
        this.capacite = 3;
    }

    public Garage(int capacite) {
        this.capacite = capacite;
    }

    public int getCapacite() {
        return capacite;
    }

    public boolean estVide() {
        return collectionVoiture.size() == 0;
    }

    public boolean estPlein() {
        return collectionVoiture.size() == capacite;
    }

    public boolean ajouter(Voiture v) {
        if (!this.estPlein()) {
            collectionVoiture.add(v);
            return true;
        }
        return false;
    }

    public int nombreDeVoitures() {
        return collectionVoiture.size();
    }

    public boolean supprimerDernier() {
        if (!this.estVide()) {
            collectionVoiture.remove(collectionVoiture.size() - 1);
            return true;
        }
        return false;
    }

    public void ranger() {
        boolean yapermut = true;
        while (yapermut) {
            yapermut = false;
            for (int i = 0; i < collectionVoiture.size() - 1; ++i) {
                if (collectionVoiture.get(i).compareTo(collectionVoiture.get(i + 1)) > 0) {
                    Voiture temp = collectionVoiture.get(i);
                    collectionVoiture.set(i, collectionVoiture.get(i + 1));
                    collectionVoiture.set(i + 1, temp);
                    yapermut = true;
                }
            }
        }
    }

    public Voiture obtenir(int x) {
        if (x <= this.capacite && x > 0) {
            return collectionVoiture.get(x - 1);
        } else {
            return null;
        }
    }

    public boolean contient(Voiture vr) {
        boolean trouve = false;
        for (Voiture vl : collectionVoiture) {
            if (vr.equals(vl)) {
                trouve = true;
            }
        }
        return trouve;
    }

    public String toString() {
        String representation = "Garage : \n";
        int i = 0;
        for (Voiture vl : collectionVoiture) {
            ++i;
            representation += "  " + i + ") " + vl.toString() + "\n";
        }
        return representation;
    }

}
