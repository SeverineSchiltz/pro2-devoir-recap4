package labo4;

import org.junit.Test;
import static org.junit.Assert.*;

public class VoitureTest {
    
    @Test
    public void testCreationVoiture(){
        Voiture v = new Voiture("Citroen", "2cv");
        assertEquals("Citroen",v.getMarque());
        assertEquals("2cv", v.getModele());
    }
    
    @Test
    public void testToString(){
        Voiture v = new Voiture("Citroen", "2cv");
        assertEquals("Citroen 2cv",v.toString());
    }
    
    @Test
    public void testEquals(){
        Voiture v1 = new Voiture("Citroen", "2cv");
        Voiture v2 = new Voiture("Citroen", "2cv");
        Voiture v3 = new Voiture("Citroen", "DS");
        assertEquals(v1,v2);
        assertFalse(v1.equals(v3));
    }
    
        
    @Test
    public void testCompareTo(){
        Voiture v1 = new Voiture("Citroen", "2cv");
        Voiture v2 = new Voiture("Citroen", "2cv");
        Voiture v3 = new Voiture("Citroen", "DS");
        assertTrue(v1.compareTo(v2)==0);
        assertTrue(v1.compareTo(v3)<0);
        assertTrue(v3.compareTo(v1)>0);
    }
    
}
