package labo4;

import java.util.Objects;

public class Voiture {
    private String marque;
    private String modele;

    public Voiture(String marque, String modele) {
        this.marque = marque;
        this.modele = modele;
    }

    public String getMarque() {
        return marque;
    }

    public String getModele() {
        return modele;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voiture voiture = (Voiture) o;
        return marque.equals(voiture.marque) &&
                modele.equals(voiture.modele);
    }

    public int compareTo(Voiture other){
        return (this.getMarque() + " " + this.getModele()).compareToIgnoreCase(other.getMarque() + " " + other.getModele());
    }

    public String toString(){
        return this.getMarque() + " " +  this.getModele();
    }
}
